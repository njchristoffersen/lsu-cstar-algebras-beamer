\documentclass{beamer}

%\newtheorem{theorem}{Theorem}[section]
%\newtheorem{lemma}[theorem]{Lemma}
%\newtheorem{corollary}[theorem]{Corollary}
%\newtheorem{proposition}[theorem]{Proposition}
%\newtheorem{noname}[theorem]{}
%\newtheorem{sublemma}{}[theorem]
%\newtheorem{conjecture}[theorem]{Conjecture}
%
%\theoremstyle{definition}
%\newtheorem{definition}[theorem]{Definition}
%\newtheorem{example}[theorem]{Example}
%
%\theoremstyle{remark}
%\newtheorem{remark}[theorem]{Remark}

\numberwithin{equation}{section}

\newcommand{\unit}{1\!\!1}


\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}




%-------------------------------
\title[Cuntz Algebra Representations]{On Abelian $C^{*}$-Algebras and One-Point Compactification}
\author{Nicholas Christoffersen}
%>>>>UNCOMMENT 1 FOR YOUR DEPARTMENT
%\newcommand{\deptname}{2}%MATHEMATICS
%<<<<<
\date{\vspace{-8cm}}

%--------------------------------



%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Overview}
\tableofcontents
\end{frame}

\section{Introduction}
\subsection{Definitions}
\begin{frame}%{Definitions} 
    \begin{definition}
      A vector space $\mathcal{A}$ over a field $\mathbb{F}$ is an \textit{algebra} if it is equipped with a multiplication, $(A,B)\mapsto AB$, such that, for all $A,B,C\in \mathcal{A}$ and $\alpha, \beta\in \mathbb{F}$,
      \begin{enumerate}
          \item<1-> $A(BC) = (AB)C$;
          \item<2-> $A(B+C) = AB + AC$; and
          \item<3-> $\alpha\beta(AB) = (\alpha A)(\beta B)$.
      \end{enumerate} 
      %group rings are actually algebras!
    \end{definition}
  \end{frame}

\begin{frame}
    \begin{definition}
      An algebra $\mathcal{A}$ is \textit{abelian}, or commutative, if the multiplication is commutative:
      \begin{align*}
          AB = BA,
      \end{align*}
      for all $A,B\in \mathcal{A}$.
    \end{definition}
      \pause
      And $\mathcal{A}$ is \textit{unital}, or has an identity, if there exists an element $\unit\in \mathcal{A}$ such that for all $A\in \mathcal{A}$,
      \[
          \unit A = A \unit = A.
      \]
\end{frame}

\begin{frame}
    \begin{block}{Examples}
        The set of $n\times n$ matrices over a field $\mathbb{F}$, denoted $M_n(\mathbb{F})$, is a non-abelian algebra with identity.
    \end{block}
\end{frame}

\begin{frame}
  \begin{definition}
      Let $\mathcal{A}$ be an algebra over $\C$. A map $^{*}:\mathcal{A}\to \mathcal{A}$, $A\mapsto A^*$ is called an \textit{involution}, or adjoint operation on $\mathcal{A}$, if for all $A, B\in \mathcal{A}$, and $\alpha, \beta\in \C$, we have
      \begin{enumerate}
          \item<1-> $A^{**} = A$;
          \item<2-> $(AB)^* = B^*A^*$; and 
          \item<3-> $(\alpha A + \beta B)^* = \overline{\alpha}A^* + \overline{\beta}B^*$,
      \end{enumerate}
      \pause
      \pause
      \pause
      where $\overline{\alpha}$ denotes the conjugate of $\alpha\in \C$.
  \end{definition}
  \pause 
  \begin{block}
      {Example}
      In the case of $M_n(\C)$, the involution is given by taking the adjoint (conjugate transpose).
  \end{block}
\end{frame}

\begin{frame}
  \begin{definition}
      An algebra $\mathcal{A}$ is a \textit{normed algebra} if there is a map 
      \begin{align*}
          A \mapsto \|A\|\in \R_+
      \end{align*}
      that sends $A$ to the norm of $A$ and has the following properties:
      \begin{enumerate}
          \item<1-> $\|A\| \geq 0$ and $\|A\| = 0$ if and only if $A = 0$;
          \item<2-> $\|\alpha A\| = \left| \alpha \right| \|A\|$; 
          \item<3-> $\| A + B \| \leq \|A\| + \|B\|$; and
          \item<4-> $\|AB\|\leq \|A\|\|B\|$. 
      \end{enumerate}
      %The third and fourth of these properties are called the \textit{triangle inequality} and \textit{product inequality}, respectively.

  \end{definition}
\end{frame}


%\begin{frame}
%\begin{definition}
%      This norm defines a topology, which is called the \textit{uniform topology}. If a normed algebra is complete with respect to its uniform topology, it is called a \textit{Banach algebra}. If, in addition, it is an involutive algebra with the property that $\|A\| = \|A^{*}\|$, then it is called a \textit{Banach $*$-algebra}.
%\end{definition}
%\end{frame}

\begin{frame}
    \begin{definition}
        An involutive Banach algebra $\mathcal{A}$ is a $C^{*}$-algebra if it satisfies the $C^{*}$-identity:
        \[
            \|A^{*}A\| = \|A^2\|.
        \]
    \end{definition}

    \pause
    \begin{block}{Examples}
        The set of $M_n(\mathbb{C})$ with the operator norm:
        \[
            \|A\| = \sup \{\|Ax\| : x\in \mathbb{C}^{n}, \|x\| = 1\} 
        \]
        is a $C^{*}$-algebra.
    \end{block}
\end{frame}

%\begin{frame}
%    These are common objects: think matrices, or more generally operators on a hilbert space
%\end{frame}
%
%\begin{frame}
%    We will focus on abelian $C^{*}$-algebras, and the following illuminating example
%\end{frame}

\begin{frame}
  \begin{definition}%\label{def:vanish-at-infinity}
      Let $f:X \to \C$ be a continuous map from a topological space $X$ to $\C$. We say that $f$ \textit{vanishes at infinity} if, for every $\epsilon > 0$, there exists a compact set $K$ such that
      \[
          |f(x)| < \epsilon, \text{ for all } x\in X\setminus K.
      \]

      The \textit{supremum norm} of $f$ is given by:
      \[
          \|f\| = \sup \{ \left| f(x) \right| : x\in X\} 
      \]
  \end{definition}
\end{frame}

\begin{frame}{Example}
    Let $X$ be a topological space. Denote $C_0(X)$ the set of continuous functions from $X$ to $\C$ that vanish at infinity. Involution is given by pointwise conjugation:
    \[
        f^{*}(x) = \overline{f(x)}.
    \]

    With the supremum norm, $C_0(X)$ is an abelian $C^{*}$-algebra. 
\end{frame}

\begin{frame}
  \begin{theorem}\label{add-identity}
      Every $C^{*}$-algebra can be embedded into a unital $C^{*}$-algebra.
  \end{theorem} 
\end{frame}

\begin{frame}
    \begin{theorem}[Construction of Unital $C^{*}$-algebra]
      If $\mathcal{A}$ does not have identity, then let $\tilde{\mathcal{A}}$ denote the algebra of pairs $\{(\alpha, A): \alpha\in C, A\in \mathcal{A}\} $ with
      \begin{itemize}
          \item<1-> $(\alpha, A) + (\beta, B) = (\alpha + \beta, A + B),$
          \item<2-> $(\alpha, A)(\beta, B) = (\alpha\beta, \alpha B + \beta A + AB)$
          \item<3-> $(\alpha, A)^{*} = (\overline{\alpha}, A^{*}), and $
          \item<4-> $\|(\alpha, A)\| = \sup \{\|\alpha B + AB\|: B\in \mathcal{A}, \|B\| = 1\}.$
      \end{itemize}
      \pause \pause \pause \pause
      Then $\tilde{\mathcal{A}}$ is a $C^{*}$-algebra with identity. 
  \end{theorem} 
\end{frame}

\begin{frame}
    In the case of $C_0(X)$, adjoining identity can be realized as the one-point compactification of the domain topological space.
\end{frame}

\begin{frame}{Example}
    Let $X = \R$. The one-point compactification of $\R$ is $S^{1}$. We can view $\R$ as an open subset of $S^{1}$, and so we can view $C_0(\R)\subseteq C_0(S^{1})$. The latter of these is a unital $C^{*}$-algebra, since $S^{1}$ is compact.
\end{frame}




\begin{frame}{References}
\begin{thebibliography}{99}
  \bibitem{brat-rob} Bratelli, Ola and Robinson, Derek W., \textit{Operator Algebras and Quantum Statistical Mechanics: $C^{*}$- and $W^{*}$-Algebras Symmetry Groups Decompositions of States}, Second edition, Springer-Verlag, New York, 1987.
\end{thebibliography}
    
\end{frame}

%%%%%%%%%%%%%%
\end{document}

